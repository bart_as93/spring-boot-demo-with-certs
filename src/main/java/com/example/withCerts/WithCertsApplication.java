package com.example.withCerts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WithCertsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WithCertsApplication.class, args);
	}

}
