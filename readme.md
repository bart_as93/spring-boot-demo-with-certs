1. Generating self-signed cert

```shell
keytool -genkeypair -alias upwork -keyalg RSA -keysize 4096 -storetype JKS -keystore upwork.jks -validity 3650 -storepass password
keytool -genkeypair -alias upwork -keyalg RSA -keysize 4096 -storetype PKCS12 -keystore upwork.p12 -validity 3650 -storepass password
```


2. Using existing cert:
```shell
keytool -import -alias springboot -file myCertificate.crt -keystore upwork.p12 -storepass password
```

3. Run spring-boot: mvn clean spring-boot:run
4. Run curl: curl https://localhost:8080/demo